\documentclass[a4paper,10pt,abstract]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}

\usepackage{fullpage}
\usepackage[bookmarks, colorlinks]{hyperref}

\usepackage{enumitem}
\usepackage[separate-uncertainty = true]{siunitx}
\sisetup{range-phrase=\ --\ , range-units=single}
\usepackage{amsmath}
\usepackage{circuitikz}

\title{Riflessioni su Laboratorio 2}
\author{Alessandro Piazza \and Niccolò Prociani}

\begin{document}

\maketitle

\begin{abstract}
    Alcune note scritte in preparazione all'esame di Laboratorio 2, incentrate principalmente sulle esperienze. Purtroppo non sono state completate e mancano alune parti rilevanti. \\

    Quest'opera è stata rilasciata con licenza Creative Commons Attribuzione-Condividi allo stesso modo 4.0 Internazionale. Per leggere una copia della licenza visita il sito web \url{http://creativecommons.org/licenses/by-sa/4.0/}.
    \href{http://creativecommons.org/licenses/by-sa/4.0/}{\includegraphics[scale=0.8]{by-sa}}
\end{abstract}

\section{Note e dettagli}
\begin{description}
    \item[Tester] Il tester digitale è molto più bello, meglio usare quello soprattutto come voltmetro. Le resistenze interne sono:
    \begin{itemize}
        \item \emph{analogico} (\texttt{ICE-680R}): come amperometro ha ua caduta di potenziale per inserzione a fondo scala di $ \Delta V_{\text{ins,fs}} \simeq \SI{300}{\milli\volt} $ (a \SI{50}{\micro\ampere} è di $ \simeq \SI{100}{\milli\volt} $); come voltmetro ha una resistenza per fondo scala di $ \SI{20}{\kilo\ohm}/V_{\text{fs}} $.
        \item \emph{digitale} (\texttt{DM-3900}): come amperometro ha ua caduta di potenziale per inserzione a fondo scala di $ \Delta V_{\text{ins,fs}} \simeq \SI{200}{\milli\volt} $; come voltmetro ha una resistenza indipendente dalla portata di \SI{10}{\mega\ohm}.
    \end{itemize}
    Gli errori di calibrazione li consideriamo come sistematici, quelli di lettura come statistici. Gli errori di calibrazione sono nel manuale (errori percentuali), quelli di lettura sono nel manuale per quello digitale (nella forma $ \pm x $ \si{digit}) e dipendono entrambi dal fondo scala. L'errore di lettura dell'analogico è di solito $ \pm $ ``mezza tacca'' ovvero $ 1/100 $ del fondo scala. Sommiamo i due tipi di errori in quadratura e di solito l'errore risultante è dominato da quello di calibrazione (controllare che sia poco superiore da quest'ultimo). \\
    In alcuni casi specifici (e.g. misure ripetute della stessa grandezza tenendo la stessa scala) si può trascurare l'incertezza di calibrazione ma la scelta deve essere debitamente motivata.

    \item[Absolute sigma] Relazione tra gli errori sui parametri di best-fit $ p_m $ con \texttt{absolute\_sigma = False/True}
    \[
        \Delta p_m\big\lvert_{\text{\texttt{False}}} = \sqrt{\chi^2_{\text{rid}}} \cdot \Delta p_m\big\lvert_{\text{\texttt{True}}}.
    \]
    Le due opzioni trattano gli errori con uno spirito diverso:
    \begin{itemize}
        \item \texttt{True} significa considerare \emph{standard errors} ovvero $ \Delta p_m $ è l'intervallo di variazione di $ p_m $ che corrisponde ad un aumento unitario del $ \chi^2 $. Tale definizione presuppone di avere errori di natura prevalentemente stocastica.
        \item \texttt{False} significa considerare \emph{asymptotic errors} ovvero l'incertezza che si avrebbe se il numero di dati in esame fosse molto grande e se l'errore avesse origine puramente stocastica. In altri termini esso rappresenta l'incertezza che sarebbe attribuita alla valutazione dei parametri se fosse $ \chi^2_{\text{rid}} \simeq 1 $. Se l'origine degli errori è poco conosciuta oppure è il contributo sistematico ad essere dominante e se siamo abbastanza fiduciosi nel nostro modelli, è \emph{consigliata} tale opzione.
    \end{itemize}
    Di solito è difficile stabilire quale sia l'opzione migliore da usare, considerando il fatto che le misure di grandezze elettriche in laboratorio sono spesso affette da errori dovuti principalmente alle incertezze di calibrazione. La cosa più conveniente è di solito vedere che cosa consiglia FF nelle note relative all'esperienza in questione.

    \item[Partitore]
    \begin{itemize}[label={--}]
        \item \emph{di tensione}: generatore ideale $ V_0 $ e due resistenze in serie $ R_1 $ e $ R_2 $. Caduta di tensione ai capi delle resistenze è $ V_i = \frac{R_i}{R_1 + R_2} V_0 $ con $ i = 1, 2 $;
        \item \emph{di correte}: generatore ideale $ V_0 $ e due resistenze in parallelo $ R_1 $ e $ R_2 $. Corrente nei due rami è $ I_i = V_0 / R_i $ con $ i = 1, 2 $.
    \end{itemize}
    Se faccio le misure con un voltmetro o amperometro reale di resistenza interna $ r $ le ddp e correnti misurate sono $ V_{i,\text{mis}} = \frac{R_i}{R_1 + R_2 + R_1 R_2 / r} V_0 $ e $ I_{i,\text{mis}} = \frac{V_0}{R_i + r} $.

    \item[ADC] Componenti fondamentali di un \emph{analog to digital converter}:
    \begin{itemize}[label={--}]
        \item \emph{clock}, dispositivo in grado di generare impulsi di durata trascurabile equispaziati;
        \item \emph{counter}, dispositivo in grado di contare gli impulsi del clock e dotato di un circuito di azzeramento comandabile;
        \item \emph{digital interface}, processore in grado di trattare il conteggio del counter e produrre un'output digitale;
        \item \emph{ramp generator}, dispositivo in grado di fornire una ddp linearmente crescente e che permetta di comandare la partenza della rampa dall'esterno (\emph{trigger});
        \item \emph{comparator}, dispositivo i grado di confrontare due ddp (riferite alla stessa linea di terra) in ingresso e fornire in uscita l'esito della comparazione (0 nel caso in cui il primo sia maggiore del secondo e 1 nel caso opposto).
    \end{itemize}
    Un modello semplicistico del comportamento è il seguente:
    \begin{enumerate}
        \item all'ingresso del comparatore sono inviate la ddp da misurare $ V_{\text{in}} $ e la ddp prodotta dal generatore di rampa $ V_{\text{ref}} $;
        \item il trigger del generatore è sincronizzato con il reset del contatore e quindi all'inizio del processo il contatore segna 0 e $ V_{\text{ref}} = 0 $;
        \item il contatore conta fino a quando $ V_{\text{in}} > V_{\text{ref}} $;
        \item non appena $ V_{\text{in}} \leq V_{\text{ref}} $ l'uscita del comparatore cambia il proprio livello, il processo si interrompe e l'interfaccia digitale acquisisce il conteggio e inizia a elaborare l'uscita digitale. Essendo la rampa lineare, il conteggio sarà proporzionale a $ V_{\text{in}} $.
    \end{enumerate}

    \item[Arduino] Come digitalizzatore:
    \begin{itemize}[label={--}]
        \item Dinamica a \SI{10}{bit}. La sensibilità della misura è di circa $ 5/1023 \sim \SI{5}{\milli\volt} $ se si usa come riferimento la ddp massima di $ \SI{5}{\volt} $, oppure di $ 1.1/1023 \sim \SI{1}{\milli\volt} $ se si usa la ddp di riferimento interna di $ \SI{1.1}{\volt} $.
        \item Resistenza interna nominale delle porte utilizzate come digitalizzatori $ \sim \SI{100}{\mega\ohm} $;
        \item Nella configurazione \emph{overcloked} si ha tempo di rampa $ t_{\text{r}} \simeq \SI{12}{\micro\second} $ ed un intervallo di digitalizzazione effettivo (tempo di rampa più tempo morto) tipicamente di $ \Delta t \simeq \SI{40}{\micro\second} $.
        \item Incertezze in digitalizzazione di \SI{4}{\micro\second} nelle misure di tempi (corroborata da deviazione standard) e \SI{1}{digit} nella misura di ddp  di (corroborata da deviazione standard solo per $ V\ped{ref} = \SI{1.1}{\milli\volt}$).
        \item Oscillatore interno al quarzo ha frequenza di \SI{16.000}{\mega\hertz} nominale.
    \end{itemize}
    Porte digitali come alimentatori:
    \begin{itemize}[label={--}]
        \item Corrente massima erogabile $ \sim\SI{20}{\milli\ampere} $ (o $ 40 $?);
        \item Resistenza interna $ \sim \SI{20}{\ohm} $ (misura à la Thévenin); possibilmente diversa a seconda che la porta sia accesa o spenta.
    \end{itemize}
    Altre osservazioni:
    \begin{itemize}
        \item Negli sketch, l'istruzione \texttt{ard.write(b'5')} dice al microcontrollore l'intervallo di campionamento \emph{nominale} in unità di \SI{100}{\micro\second}, per cui \texttt{5} significa che i dati vengono campionati ogni \SI{500}{\micro\second}. L'intervallo di campionamento deve essere modificato in base alle necessità, ma si consiglia di non scendere sotto i \textcolor{red}{?}. \textcolor{red}{\textbf{Achtug!} In alcune esperienze è in unità di \SI{10}{\micro\second}.}
        \item Il segnale in \texttt{PWM} ha frequenza fissa nominale di \SI{488}{\hertz} per le porte \texttt{$ \sim $3}, \texttt{$ \sim $9}, \texttt{$ \sim $10} e di \SI{976}{\hertz} per le porte \texttt{$ \sim $5}, \texttt{$ \sim $6} e duty-cycle regolabile su \SI{8}{bit}, ovvero 256 livelli.
    \end{itemize}

    \item[Oscilloscopio] La banda passante è $ \sim\SI{50}{\mega\hertz} $. Il frequenzimetro in basso a destra è riferito alla frequenza di trigger.

    \item[Misure RMS] Per segnali a media nulla, se $ f_0 $ è l'ampiezza dell'onda i valori RMS (teorici) risultano pari a:
    \begin{itemize}[label={--}]
        \item Sinusoidale: $ \frac{f_0}{\sqrt{2}} $
        \item Quadra: $ f_0 $
        \item Triangolare: $ \frac{f_0}{\sqrt{3}} $
    \end{itemize}
    Il multimetro, invece, misura il valore ottenuto inviando il segnale originale ad un raddrizzatore a semionda e poi ad un integratore, ovvero $ \frac{2}{\pi}f_0 $ per segnali a media nulla, e lo moltiplica poi per un fattore correttivo pari a $ \frac{\pi}{2\sqrt{2}} $.\\
    A causa della presenza dell'integratore la misura RMS è valida solo in determinati regimi in frequenza, per il multimetro nominalmente \SIrange{0}{400}{\hertz}

    \item[Filtri RC] La frequenza di taglio è $ f\ped{T} = \frac{1}{2\pi R C} $.
    \begin{itemize}
        \item \emph{Passa basso}: a frequenze basse il condensatore è un circuito aperto da cui $ V_{\omega,\text{out}} \simeq V_{\omega,\text{in}} $, a frequenze alte il condensatore è un cortocircuito da cui $ V_{\omega,\text{out}} \simeq 0 $. Funzione di trasferimento e sfasamento sono
        \[
            T(f) = \frac{1}{1 + j f/f\ped{T}} \qquad \tan(\Delta \varphi) = -\frac{f}{f\ped{T}}
        \]
        \item \emph{Passa alto}: a frequenze basse il condensatore è un circuito aperto da cui $ V_{\omega,\text{out}} \simeq 0 $, a frequenze alte il condensatore è un cortocircuito da cui $ V_{\omega,\text{out}} \simeq V_{\omega,\text{in}} $. Funzione di trasferimento e sfasamento sono
        \[
            T(f) = \frac{1}{1 - j f\ped{T}/f} \qquad \tan(\Delta \varphi) = \frac{f\ped{T}}{f}
        \]
    \end{itemize}

    \item[Integratore/derivatore RC] La funzione di un filtro RC come integratore (risp. derivatore) funziona bene solo nel caso in cui la forzante sia un'onda quadra o sinusoidale e nell'approssimazione $ f \gg f\ped{T} $ (risp. $ f \ll f\ped{T} $).

    \item[Condizioni di non perturbazione] Un modo per determinare le condizioni di non perturbazione tra circuiti in cascata, in particolare per la cascata integratore - derivatore, è di imporre che l'impedenza di uscita del primo circuito sia molto minore dell'impedenza di ingresso del secondo.

    \item[Decibel] Per le potenze il valore in decibel è:
    \[ 10\log\ped{10}\left(\frac{P}{P\ped{ref}}\right) \]
    Per campi, ddp e ampiezze in generale:
    \[ 20\log\ped{10}\left(\frac{A}{A\ped{ref}}\right) \]
    Nel caso specifico del guadagno/attenuazione, $ A\ped{ref} = 1 $.

\end{description}

\section{Domande e riflessioni}
\begin{description}
    \item[3 - ardu\_hist] Nel confronto tra il tester digitale e Arduino (paragrafo VII) afferma ``a resistenza di ingresso di Arduino è superiore rispetto a quella (già molto alta) del multimetro digitale usato come voltmetro, e questo potrebbe aumentare l’ampiezza della d.d.p. creata dal disturbo''. Cosa vuol dire?

    \item[Auto e mutua induzione]
    \begin{itemize}[label={--}]
        \item frequenza di taglio determinata come nel caso degli RC
        \item domande di "stimolo".
    \end{itemize}

    \item[Ponti] come realizzare un componente con induttanza variabile.
\end{description}

\section{Esperienze}
\begin{description}
    \item[2 - Misure alla Thévenin] Modello il generatore reale (\emph{black box}) come la serie tra un generatore ideale di ddp $ V_{\text{th}} $ e una resistenza $ R_{\text{th}} $. $ V_{\text{th}} $ si misura collegando al generatore ad un voltmetro ideale (tester digitale va bene); la misura viene quindi fatta a circuito aperto. $ R_{\text{th}} $ si misura costruendo un partitore di tensione: attacco al generatore reale una resistenza di carico nota $ R_L $ e misuro con un voltmetro ideale la ddp $ V_L $, così allora $ R_{\text{th}} = R_L (V_{\text{th}} / V_L - 1) $. La resistenza interna ``alla Thévenin'' viene valutata con una sola misura e quindi ha solitamente un errore abbastanza alto (nell'Esperienza 2 abbiamo ottenuto $ \sim 4-5\% $).

    \item[2 - Tester] Tieni conto della resistenza interna del tester; tieni il più possibile scollegate le cose a resistenze basse per evitare surriscaldamenti.

    \item[2 - Resistenza del generatore] Circuito formato da generatore reale di ddp $ V_0 $ e resistenza $ r_G $, in serie con una resistenza $ R $ e un amperometro reale di resistenza $ r_A $. La funzione di best fit consigliata è \[ I(R) = \frac{V_0}{r_G + r_A + R} \] dove i parametri liberi sono $ V_0 $ e $ r_G $ e in realtà $ r_A = r_A(R) $. In altri termini si consiglia di \emph{non} trascurare la resistenza dell'amperometro ma di aggiungere a $ R $ il valore nominale, quindi senza incertezza, di tale resistenza (che dipende dal fondo scala $ \SI{200}{\milli\volt}/I $ e quindi da $ R $). Data l'origine prevalentemente sistematica degli errori, si consiglia l'opzione \texttt{absolute\_sigma = False}. \\
    Eventuali discrepanze rispetto a quanto atteso, soprattutto basse resistenze/alte correnti, possono essere dovute al surriscaldamento dei componenti resistivi che ne modifica la resistenza.

    \item[3 - DC Arduino] Cosa intende Fuso quando chiede di ``confrontare i due metodi di calibrazione''? Probabilmente bisogna confrontare le bande di confidenza che si trovano con le due calibrazioni (ricordarsi che per la calibrazione fatta con il fit sul modello $ V = \alpha + \beta X $ bisogna tenere conto della covarianza $ \delta V\ped{prev} = \sqrt{\sigma^2_\alpha + \sigma^2_\beta X^2 + 2 \sigma_{\alpha\beta} X} $). La discrepanza tra le due calibrazioni si nota principalmente per bassi valori di ddp, a causa dell'offset non presente nella calibrazione ``alternativa''. \\
    Digitalizzando l'uscita digitale di Arduino con il digitalizzatore di Arduino stesso si misura \SI{1008}{digit} invece dell'atteso \SI{1023}{digit}.

    \item[4 - Carica Scarica]
    \begin{itemize}
        \item È importante dimensionare bene il circuito per riuscire a campionare bene il segnale e non bruciare Arduino. In particolare è consigliato avere $ R > \SI{1}{\kilo\ohm} $ e avere un intervallo di campionamento complessivo $ {\Delta t}\ped{tot} \simeq 256 \times \Delta t \sim \zeta \tau $ con $ \zeta \sim 2-3 $ dove $ \Delta t $ è il tempo di digitalizzazione e $ \tau = 1/RC $. FF dice di aver usato $ \Delta t = \SI{100}{\micro\second} $, $ R = \SI{6.8}{\kilo\ohm} $ e $ C = \SI{1}{\nano\farad} $ (valori nominali).
        \item Non è necessario convertire le ddp misurate da Arduino da \si{digit} a unità fisiche, ma allora nel modello di fit è opportuno aggiungere un offset per tenere conto della calibrazione. L'offset è rilevante nel caso della scarica, dato che si prendono tante ddp vicine allo 0, ma secondo FF non lo è per la carica (forse perché si prendono pochi dati vicino allo 0).
        \item I tempi caratteristici in carica e scarica potrebbero essere non compatibili; in caso è dovuto alla diversa resistenza interna della porta dell'Arduino in stato ``acceso'' e ``spento''.
        \item Cosa c'è da dire nel punto 11?
        \item In questa esperienza si vede quanto fa schifo Arduino come digitalizzatore, in particolare nel grafico dei residui si notano:
        \begin{itemize}
            \item \emph{spikes} che distano anche più di $ 4\sigma $ dal fit, solitamente quando la ddp è prossima a potenze di 2; sono probabilmente dovuti a problemi/latenze nel circuito di \emph{sample and hold} e possono essere considerati (motivando opportunamente) come \emph{outliers}.
            \item ``creste'' con andamenti esponenziali particolarmente visibili nelle parti terminali di carica e scarica (soprattutto se si acquisiscono record molto lunghi, $ \zeta \geq 7-8 $) principalmente dovuti alla scarsa sensibilità del digitalizzatore (le ddp digitalizzate sono costanti in blocchi molto lunghi di tempo).
        \end{itemize}
    \end{itemize}

    \item[5 - Generatore e Arduino] In alcuni casi si verifica incompatibilità tra la misura della frequenza del segnale data dal frequenzimetro del generatore e la frequenza che si trova facendo un fit dei dati digitalizzati da Arduino (le due ``misure'' di $ f $ portano a valori ``simili'', ma non compatibili entro le incertezze). Quando confrontiamo queste due misure stiamo implicitamente assumendo che i due strumenti siano correttamente calibrati rispetto alla misura \emph{assoluta} dei tempi. Essendo la frequenza dell'oscillatore al quarzo di Arduino di \SI{16.000}{\mega\hertz} nominali, possiamo assumere che la misura di tempi sia calibrata con un'accuratezza dell'ordine di 1 parte su $ 10^4 $. Similmente la misura di tempi del frequenzimetro del generatore di funzioni è calibrata entro 20 ppm ovvero a 2 parti su $ 10^5 $, supponendo che
    lo strumento sia correttamente ``termalizzato''. A prevalere è l’incertezza (assoluta e sistematica) di Arduino, e in effetti le discrepanze relative riscontrate tra le due ``misure'' di frequenza sono spesso dell’ordine di 1 parte su $ 10^4 $.

    \item[6 - Integro/derivo] Cascata integratore(A)-derivatore(B), ovvero \emph{passa banda}:
    \begin{itemize}[label={--}]
        \item condizione sulla frequenza di lavoro $ f\ped{TA} \ll f \ll f\ped{TB} $;
        \item la condizione di indipendenza tra i due filtri è $ C\ped{B} \ll C\ped{A} $;
        \item per eliminare il ``rumore ad alta frequenza'' introduco il condensatore da $ \sim  \SI{1}{\nano\farad} $ in parallelo al segnale. Come si modellizza?
        \item Nel punto 9, che cos'è lo sfasamento $ {\Delta \varphi}\ped{A} $? Sto confrontando un'onda quadra e una triangolare.
    \end{itemize}

    \item[7 - Filtri RC]
    \begin{itemize}[label={--}]
        \item Nel punto 2 cosa si intende con ``ricordando che si tratta di misure condizionate''?
        \item Nel punto 4 che cosa si dovrebbe vedere in modalità \texttt{XY}?
        \item La resistenza interna del generatore non entra nella funzione di trasferimento ma fa solo in modo che $ V\ped{in} $ cambi con la frequenza.
    \end{itemize}

    \item[8 - Curva caratteristica del diodo]
    \begin{itemize}[label={--}]
        \item Misurando la resistenza del diodo con il multimetro digitale configurato come ohmetro ci si aspetta che la lettura aumenti all'aumentare del fondo scala. Un ohmetro digitale infatti inietta una corrente nota nel componente e misura la differenza di potenziale, ed è ragionevole aspettarsi che, per mantenere $ V $ entro valori ragionevoli, $ I $ venga diminuita all'aumentare del fondo scala\footnote{Si osserva sperimentalmente che ciò non è vero nel passaggio dal fondoscala di $ \SI{20}{\mega\ohm} $ a quello di $ \SI{200}{\mega\ohm} $}. Poiché in un diodo operato a corrente fissa il rapporto $ \frac{V}{I} $ aumenta al'aumentare della corrente, ci si aspetta che all'aumentare del fondo scala la lettura aumenti.
        \item L'ampiezza del ripple aumenta al diminuire della resistenza di carico, perché se al condensatore è richiesta tanta corrente questo si scarica sensibilmente nell'arco di un ciclo.
        \item Per contro, aumentare la resistenza di carico diminuisce il range di ddp effettivamente esplorabili ai capi del diodo. Per ddp maggiori di $ \simeq \SI{0.6}{\volt} $ l'intensità di corrente nel diodo diventa molto alta e pertanto la caduta sulla serie $ R + R_D $ diventa tutt'altro che trascurabile.
        \item Scegliendo una capacità abbastanza alta il ripple potrebbe non essere visibile. È comunque visibile del rumore, che però è a frequenza molto più alta di quella prevista per il ripple ($ \SI{976}{\hertz} $)
        \item Se si misura il valore all'uscita dell'integratore per effettuare la calibrazione quando il diodo è collegato ci sarà una discrepanza con il valore misurato senza diodo, dovuto alla corrente che scorre nella serie di $ R $, $ R_D $ e diodo (al posto di $ V_0 = \SI{5}{\volt} $ si dovrebbe vedere $ \frac{R_D V_0 + R V\ped{thr}}{R + R_D} $ dove si è stimata la caduta di potenziale ai capi del diodo come costate pari $ V\ped{thr} \simeq \SI{0.6}{\volt} $); tale discrepanza dovrebbe scomparire misurando invece a monte dell'integratore.
        \item Valori consigliati (nominali): $ R = \SI{330}{\ohm} $, $ C = \SI{100}{\micro\farad} $ così da avere una $ f\ped{T} = \SI{4.8}{\hertz} $; $ R_D = \SI{680}{\ohm} $ abbastanza alta per limitare la corrente richiesta ad Arduino e diminuire il ripple, ma non troppo per permettere di esplorare un buon range di $ \Delta V $.
    \end{itemize}

    \item[8 - Raddrizzatore a Semionda] Il ``raddrizzatore a semionda'' effettivo è composto solo da generatore, diodo e condensatore; la resistenza in parallelo al condensatore è il carico e non parte del rettificatore.

    \item[9 - Resistenza dinamica del diodo]
    \begin{itemize}[label={--}]
        \item Nella misura di $ v_d $ al variare di $ R_P $ è bene cercare di tenere $ v_d $ costante (attorno ai $ \SI{5}{\milli\volt} $) diminuendo $ V_G $, in modo da ridurre l'errore relativo.
        \item Per bassi valori della resistenza dinamica la resistenza ohmica dei contatti non è trascurabile.
        \item La resistenza interna del generatore alternato deve essere trascurabile rispetto alla prima resistenza del partitore e alla serie di entrambe le resistenze del partitore. Ciò è ragionevolmente verificato.
        \item La resistenza interna del generatore continuo non compare perché la ddp è misurata a valle.
        \item In modulo l'impedenza del condensatore è ragionevolmente piccola rispetto alla resistenza di Thévenin.
        \item Per vedere se ho corrente AC nella maglia DC la resistenza della maglia DC va confrontata con quella del diodo. Idem per il canale dell'oscilloscopio che legge il diodo.
        \item La resistenza interna del canale dell'oscilloscopio che legge il generatore non entra in $ R\ped{Th} $. Potrebbe cambiare la misura di $ V\ped{Th} $ ma è trascurabile.
        \item Con un po' di conti si trova che l'errore relativo compiuto nell'approssimare la secante con la tangente nel valutare la resistenza dinamica \emph{non} dipende dal punto di lavoro, ma solo dal rapporto $ v_d/\eta V_T $.
    \end{itemize}
\end{description}

\end{document}
