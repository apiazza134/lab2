Riflessioni e note scritte in preparazione all'esame di Laboratorio 2 (principalmente incentrate sulle esperienze), purtroppo mai completate. In particolare risultano mancanti i seguenti argomenti rilevanti:
- [ ] Transistor BJT
- [ ] Amplificatore e oscillatore a rezione
- [ ] Risonanze RLC
- [ ] Ottica fisica

[Link](https://gitlab.com/apiazza134/lab2/-/jobs/artifacts/master/raw/Laboratorio2.pdf?job=pdf)  al `.pdf`.
